CREATE OR REPLACE VIEW building_meters_static AS 
WITH
  ths AS (
   SELECT
     time
   , "substr"(tag, 1, 3) building_abbreviation
   , "substr"(tag, 5, 1) utility
   , "substr"(tag, 6, 4) meter
   , "substr"(tag, 11, 2) acquisition_type
   , value
   , CAST("to_unixtime"(CAST(time AS timestamp)) AS bigint) unix_time
   FROM
     ut_power.telemetry_buildings_historian_static
) 
, bm AS (
   SELECT
     building_abbreviation
   , building_number
   FROM
     ut_power.building_metadata
) 
SELECT
  time
, bm.building_abbreviation
, bm.building_number
, utility
, meter
, acquisition_type
, value
, unix_time
FROM
  (ths 
INNER JOIN bm ON (bm.building_abbreviation = ths.building_abbreviation))
where meter not like '%_TBU%'
ORDER BY time DESC
