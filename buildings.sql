CREATE OR REPLACE VIEW buildings AS 
WITH
  buildings_static AS (
   WITH
     bm AS (
      SELECT *
      FROM
        building_metadata
   ) 
,    ths AS (
      SELECT
        CAST(time AS timestamp) time
      , "substr"(building, 1, 3) building_name
      , "substr"(building, 5, 1) utility
      , "substr"(building, 11, 2) acquisition_type
      , value
      , CAST("to_unixtime"(CAST(time AS timestamp)) AS bigint) unix_time
      FROM
        ut_power.telemetry_historian_static
   ) 
   SELECT
     time
   , ths.building_name
   , building_number
   , utility
   , acquisition_type
   , value
   , unix_time
   FROM
     (bm
   INNER JOIN ths ON (bm.building_abbreviation = ths.building_name))
) 
, buildings_dynamic AS (
   WITH
     todb AS (
      SELECT
        "date"("date_trunc"('day', "from_unixtime"((col0 / 1000)))) time
      , "substr"(col1, 1, 4) building_number
      , "substr"(col1, 6, 1) utility
      , "substr"(col1, 12, 2) acquisition_type
      , col2 value
      , col1 tag
      , col0 unix_time
      FROM
        ut_power.telemetry_opc_dynamic
      WHERE ((("codepoint"(CAST("substr"(col1, 1, 1) AS varchar(1))) >= 48) AND ("codepoint"(CAST("substr"(col1, 1, 1) AS varchar(1))) < 65)) AND ("substr"(col1, 12, 2) = 'PD'))
   ) 
,    bm AS (
      SELECT *
      FROM
        building_metadata
   ) 
   SELECT
     time
   , building_abbreviation building_name
   , todb.building_number
   , utility
   , acquisition_type
   , value
   , unix_time
   FROM
     (todb
   INNER JOIN bm ON (todb.building_number = bm.building_number))
) 
SELECT *
FROM
  buildings_static
ORDER BY time ASC
