CREATE OR REPLACE VIEW weather_static AS 
WITH
  wfs AS (
   SELECT
     "date"("concat"(CAST(year AS varchar), '-', CAST(month AS varchar), '-', CAST(day AS varchar))) time
   , temperature
   FROM
     ut_power.weather_ftp_static
) 
, wfsu AS (
   SELECT
     *
   , (CAST("to_unixtime"("date"(time)) AS bigint) * 1000) unix_time
   FROM
     wfs
) 
SELECT *
FROM
  wfsu
