-- CREATE OR REPLACE VIEW "buildings_static" AS
SELECT
   cast(time as timestamp) as time,
   "substr"(tag, 1, 3) building_name,
   "substr"(tag, 5, 1) utility,
   "substr"(tag, 11, 2) acquisition_type,
   value,
   CAST("to_unixtime"(CAST(time AS timestamp)) AS bigint) unix_time 
FROM
   ut_power.telemetry_historian_static limit 1