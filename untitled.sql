weather_dynamic AS (
	SELECT
	  "date"("date_trunc"('day', "from_unixtime"((time / 1000)))) time
	, hdd
	, cdd
	, avg_temperature temperature
	, time unix_time
	FROM
	  ut_power.weather_ftp_dynamic
)