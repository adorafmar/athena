-- CREATE OR REPLACE VIEW weather AS 
WITH
  weather_dynamic AS (
   SELECT
     "date"("date_trunc"('day', "from_unixtime"((time / 1000)))) time
   , hdd
   , cdd
   , avg_temperature temperature
   , time unix_time
   FROM
     ut_power.weather_ftp_dynamic
) 
, wfs AS (
   SELECT
     "date"("concat"(CAST(year AS varchar), '-', CAST(month AS varchar), '-', CAST(day AS varchar))) time
   , temperature
   FROM
     ut_power.weather_ftp_static
) 
, wfsu AS (
   SELECT
     *
   , (CAST("to_unixtime"("date"(time)) AS bigint) * 1000) unix_time
   FROM
     wfs
) 
SELECT
  time
, temperature
, unix_time
FROM
  wfsu
UNION SELECT
  time
, temperature
, unix_time
FROM
  weather_dynamic
